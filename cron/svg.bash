#!/bin/bash

# script de sauvegarde de l'ensemble des bases de données du site.
# Configuration : en "csv"
# - chaque base séparés par ";"
# - le nom humain (pour les log) de la base suivit de ":",
# - les noms séparé par des "," dans l'ordre de : url, nom de la base , user, mot de passe
# NOTE : l'url est écrite directement, les autres sont les chemins des fichiers contenant ces données. (secret docker)
#
# exemple :
# CRON_CONFIG = "première base:BASE_URL,BASE_NAME_FILE,BASE_USER_FILE,BASE_PW_FILE;deuxième base:BASE_URL,BASE_NAME_FILE,BASE_USER_FILE,BASE_PW_FILE;"

# TODO utiliser un fichier de mot de passe : PGPASSFILE=$WIKI_PW_FILE
# mais il est possible que le fichier de mot de passe doivent être au format : `hostname:port:database:username:password`



#docker exec postgres pg_dump -U postgres -F t my_db | gzip >/backups/my_db-$(date +%Y-%m-%d).tar.gz
#docker exec -t postgres bash -c 'pg_dump -U postgres -F t my_db | gzip >/backups/my_db-$(date +%Y-%m-%d).tar.gz'
#docker run -it  -e PGPASSWORD=my_password postgres:alpine  pg_dump  -h hostname -U my_user my_db > backup.sql
#docker exec -t postgres bash -c 'pg_dump -U postgres -F t my_db | gzip >/backups/my_db-$(date +%Y-%m-%d).tar.gz'
#docker run -it --rm -e PGPASSWORD=my_password postgres:alpine pg_dump -h hostname -U my_user my_db > backup.sql0


SVG_DIR="/backup/"

# raz [REPO] [NB]
# ne conserve dans le répertoire $1 que les $2 derniers éléments
function raz() {
	[ -n "$TEST" ] && echo "ne conserve dans le répertoire $1 que les $2 derniers éléments"
	find $1 -maxdepth 1 -type f | sort -n | head -n -$2 | xargs rm -f
}

# svg(password_file,user_file,db_file,hostname)
function svg() {
	#BASE_URL,BASE_NAME_FILE,BASE_USER_FILE,BASE_PW_FILE
	local HOSTNAME=$1
	local DB=$2
	local USER=$3
	local PW=$4

	local annee=$(date +%Y)
	local semaine=$(date +%W)
	local mois=$(date +%m)
	local jour=$(date +%d)
	local heure=$(date +%H)

	local DIR="$SVG_DIR$DB"
	local name=$(date +%Y-%m-%d_%H%M%S)-$DB.sql.gz.crypt

	mkdir -p $DIR/all $DIR/annees $DIR/semaines $DIR/mois $DIR/jours $DIR/heures
	# pour test :
	[ -n "$TEST" ] && mkdir -p $DIR/minutes

	# --------------- Crée la sauvegarde (compressée et chiffée) --------------- #
	# FIXME tester le chiffrement avec -r ou --passphrase
	# pour déchiffrer gpg -d folder.tar.gz.gpg -> trouver comment utiliser une clef

	[ -n "$TEST" ] && echo "pg_dump --clean --if-exists --host=$HOSTNAME --username=$USER --dbname=$DB"
	PGPASSWORD=$PW pg_dump -h $HOSTNAME -U $USER $DB | gzip | openssl enc -aes-256-cbc -md sha512 -pbkdf2 -salt -pass pass:$PW  > $DIR/all/$name

	# ----------------- indexe dans les répertoires temporels ------------------ #
	# Note : ne créera pas d'indexe s'il existe déjà une sauvegarde. Ainsi seul la plus ancienne est indexée
	# On efface la console sur ces instructions, car le fonctionnement normal c'est d'être bloqué par un fichier existant.
	{
		ln $DIR/all/$name $DIR/annees/${annee}
		ln $DIR/all/$name $DIR/semaines/${annee}-${semaine}
		ln $DIR/all/$name $DIR/mois/${annee}-${mois}
		ln $DIR/all/$name $DIR/jours/${annee}-${mois}-${jour}
		ln $DIR/all/$name $DIR/heures/${annee}-${mois}-${jour}-${heure}
		# pour test
		[ -n "$TEST" ] && ln $DIR/all/$name $DIR/minutes/${annee}-${mois}-${jour}-${heure}-$(date +%M)

	} &> /dev/null

	# ----------- ne conserve que le nombre de sauvegardes désirées ------------ #

	# Recherche les fichiers dans un répertoire | tris alphabétique (donc par temps) | tous sauf les x derniers | efface les fichiers listés

	if [ -n "$NB_ANNEES" ];   then raz $DIR/annees/ $NB_ANNEES; fi
	if [ -n "$NB_SEMAINES" ]; then raz $DIR/semaines/ $NB_SEMAINES; fi
	if [ -n "$NB_MOIS" ];     then raz $DIR/mois/ $NB_MOIS; fi
	if [ -n "$NB_JOUR" ];     then raz $DIR/jours/ $NB_JOUR; fi
	if [ -n "$NB_HEURE" ];    then raz $DIR/heures/ $NB_HEURE; fi

	# pour test
	[ -n "$TEST" ] && [ -n "$NB_MINUTE" ] && raz $DIR/minutes/ $NB_MINUTE


	# efface tous les fichiers qui ne possèdent pas de copie ailleurs -links 1 => les fichiers qui n'ont qu'un seul lien dur
	# quand on peut, il est préférable d'utiliser -print0 et xargs -0
	find $DIR/all/ -maxdepth 1 -type f -links 1 -print0 | xargs -0 rm -f
	echo "svg $DB"

}

# ------------ Variables (test et initialisation) ------------ #

[ -z "$CRON_CONFIG" ] && echo "Configuration introuvable" && exit 1

# Transformation de la config en tableau.
# BASES => une lignes de configs par base ( séparés par ; )
readarray -td ";" BASES < <(tr -d "\n " <<< $CRON_CONFIG) # split dans un tableau par le séparateur ;
for LIGNE in "${BASES[@]}"
do
 	# Nom humain de la base de donnée (pour les logs)
	NOM=${LIGNE%:*}

	# Extraction du tableau des 4 noms de variables d'environnement ( séparés par des , )
	readarray -td "," PARAM < <(echo -n "${LIGNE#*:}")
	[ -n "$TEST" ] && \
		echo "config : '$LIGNE'" && \
		echo "    nom : $NOM" && \
		echo "    host : '${PARAM[0]}'" && \
		echo "    db : '${PARAM[1]}'" && \
		echo "    user : '${PARAM[2]}'" && \
		echo "    pw: '${PARAM[3]}'"

	if [ ${#PARAM[@]} -ne 4 ] || [ -z "${PARAM[0]}" ] || [ -z "${PARAM[1]}" ] || [ -z "${PARAM[2]}" ] || [ -z "${PARAM[3]}" ]
	then
		echo "Base $NOM non sauvée : Configuration incorrecte : ${PARAM[@]}"
		continue
	fi

	# Transformation du tableau des noms en tableau des valeurs
	# Si le nom se termine par _FILE => extraction du fichier indiqué, si non lecture de la valeur (pointeur).
	[[ "${PARAM[0]##*_}" == "FILE" ]] && PARAM[0]=$(cat ${!PARAM[0]}) || PARAM[0]=${!PARAM[0]} # ${!$NOM} : $NOM est le nom de la variable à lire. C'est un pointeur. ( l'écriture ${$NOM} ne fonctionne pas )
	[[ "${PARAM[1]##*_}" == "FILE" ]] && PARAM[1]=$(cat ${!PARAM[1]}) || PARAM[1]=${!PARAM[1]}
	[[ "${PARAM[2]##*_}" == "FILE" ]] && PARAM[2]=$(cat ${!PARAM[2]}) || PARAM[2]=${!PARAM[2]}
	[[ "${PARAM[3]##*_}" == "FILE" ]] && PARAM[3]=$(cat ${!PARAM[3]}) || PARAM[3]=${!PARAM[3]}

	[ -n "$TEST" ] && \
		echo "Valeurs :" && \
		echo "    host : '${PARAM[0]}'" && \
		echo "    db : '${PARAM[1]}'" && \
		echo "    user : '${PARAM[2]}'" && \
		echo "    pw: '${PARAM[3]}'"

	if [ -z "${PARAM[0]}" ] || [ -z "${PARAM[1]}" ] || [ -z "${PARAM[2]}" ] || [ -z "${PARAM[3]}" ]
	then
		echo "Base $NOM non sauvée : Une variable d'environnement n'est pas définie"
		continue
	fi

	svg "${PARAM[@]}"
	unset $NOM $PARAM
done
