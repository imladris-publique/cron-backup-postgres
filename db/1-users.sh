#!/bin/bash
set -e

# script de création de l'ensemble des bases de données.
# Configuration : en "csv" identique à celle du cron (incluant le hostname meme s'il ne sert pas ici)
# - chaque base séparés par ";"
# - le nom humain (pour les log) de la base suivit de ":",
# - les noms séparé par des "," dans l'ordre de : url, nom de la base , user, mot de passe
# NOTE : l'url est écrite directement, les autres sont les chemins des fichiers contenant ces données. (secret docker)
#
# exemple :
# DB_CONFIG = "première base:BASE_URL,BASE_NAME_FILE,BASE_USER_FILE,BASE_PW_FILE;deuxième base:BASE_URL,BASE_NAME_FILE,BASE_USER_FILE,BASE_PW_FILE;"



# svg(hostname,base_name,user,password)
function creation() {
	# formatage propre à pgsql (escape des ' en les doublant)
	local HOSTNAME=$(sed "s/'/''/g" <<< $1)
	local DB=$(sed "s/'/''/g" <<< $2)
	local USER=$(sed "s/'/''/g" <<< $3)
	local PW=$(sed "s/'/''/g" <<< $4)

	# --------------- Crée la base --------------- #

	[ -n "$TEST" ] && echo <<-EOSQL
		CREATE USER "$USER" WITH ENCRYPTED PASSWORD '$PW';
		CREATE DATABASE "$DB";
		GRANT ALL PRIVILEGES ON DATABASE "$DB" TO "$USER";
		EOSQL

	psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
		CREATE USER "$USER" WITH ENCRYPTED PASSWORD '$PW';
		CREATE DATABASE "$DB";
		GRANT ALL PRIVILEGES ON DATABASE "$DB" TO "$USER";
	EOSQL

}

# ------------ Variables (test et initialisation) ------------ #

[ -z "$POSTGRES_USER" ] && echo "POSTGRES_USER non défini" && exit 1
[ -z "$POSTGRES_DB" ] && echo "POSTGRES_DB non défini" && exit 1
[ -z "$DB_CONFIG" ] && echo "Configuration introuvable" && exit 1

# Transformation de la config en tableau.
# BASES => une lignes de configs par base ( séparés par ; )
readarray -td ";" BASES < <(tr -d "\n " <<< $DB_CONFIG) # split dans un tableau par le séparateur ;
for LIGNE in "${BASES[@]}"
do
 	# Nom humain de la base de donnée (pour les logs)
	NOM=${LIGNE%:*}

	# Extraction du tableau des 4 noms de variables d'environnement ( séparés par des , )
	readarray -td "," PARAM < <(echo -n "${LIGNE#*:}")
	[ -n "$TEST" ] && \
		echo "config : '$LIGNE'" && \
		echo "    nom : $NOM" && \
		echo "    host : '${PARAM[0]}'" && \
		echo "    db : '${PARAM[1]}'" && \
		echo "    user : '${PARAM[2]}'" && \
		echo "    pw: '${PARAM[3]}'"

	if [ ${#PARAM[@]} -ne 4 ] || [ -z "${PARAM[0]}" ] || [ -z "${PARAM[1]}" ] || [ -z "${PARAM[2]}" ] || [ -z "${PARAM[3]}" ]
	then
		echo "Base $NOM non créée : Configuration incorrecte : ${PARAM[@]}"
		continue
	fi

	# Transformation du tableau des noms en tableau des valeurs
	# Si le nom se termine par _FILE => extraction du fichier indiqué, si non lecture de la valeur (pointeur).
	[[ "${PARAM[0]##*_}" == "FILE" ]] && PARAM[0]=$(cat ${!PARAM[0]}) || PARAM[0]=${!PARAM[0]} # ${!$NOM} : $NOM est le nom de la variable à lire. C'est un pointeur. ( l'écriture ${$NOM} ne fonctionne pas )
	[[ "${PARAM[1]##*_}" == "FILE" ]] && PARAM[1]=$(cat ${!PARAM[1]}) || PARAM[1]=${!PARAM[1]}
	[[ "${PARAM[2]##*_}" == "FILE" ]] && PARAM[2]=$(cat ${!PARAM[2]}) || PARAM[2]=${!PARAM[2]}
	[[ "${PARAM[3]##*_}" == "FILE" ]] && PARAM[3]=$(cat ${!PARAM[3]}) || PARAM[3]=${!PARAM[3]}

	[ -n "$TEST" ] && \
		echo "Valeurs :" && \
		echo "    host : '${PARAM[0]}'" && \
		echo "    db : '${PARAM[1]}'" && \
		echo "    user : '${PARAM[2]}'" && \
		echo "    pw: '${PARAM[3]}'"

	if [ -z "${PARAM[0]}" ] || [ -z "${PARAM[1]}" ] || [ -z "${PARAM[2]}" ] || [ -z "${PARAM[3]}" ]
	then
		echo "Base $NOM non créée : Une variable d'environnement n'est pas définie"
		continue
	fi

	creation "${PARAM[@]}"
	unset $NOM $PARAM
done
