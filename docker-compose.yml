version: '3.5'

networks:
  traefik: # réseau du reverse proxy utilisé pour sécuriser l'accès web au wiki.
    external:
      name: traefik
  reseau_interne: # réseau entre le wiki et sa base de donnée
    internal: true
  sortie: # réseau sortant pour le smtp (traefik ne permet pas les connexions de sortie)
    internal: false

# --------------------------------------------------------- #
#                        Secrets
# --------------------------------------------------------- #

secrets:
  db_pw:
    file: ./secrets/db.pw
  db_util:
    file: ./secrets/db_util.secret
  db_name:
    file: ./secrets/db_name.secret
  db_pw-wiki:
    file: ./secrets/db.wiki.pw
  db_util-wiki:
    file: ./secrets/db_util.wiki.secret
  db_name-wiki:
    file: ./secrets/db_name.wiki.secret

# --------------------------------------------------------- #
#                         Volumes
# --------------------------------------------------------- #

volumes:
  db:
  db_svg:
    driver: local
    driver_opts:
      type: none
      device: ./sauvegardes
      o: bind
  logs:
    driver: local
    driver_opts:
      type: none
      device: ./logs
      o: bind
  images:
    driver: local
    driver_opts:
      type: none
      device: ./images
      o: bind
  images-public:
    driver: local
    driver_opts:
      type: none
      device: ./images/public
      o: bind
  images-discret:
    driver: local
    driver_opts:
      type: none
      device: ./images/discret
      o: bind
  wiki-modules:
    #external:
    #  name: extensions-wiki
    driver: local
    driver_opts:
      type: none
      device: ./wiki/extensions
      o: bind

# --------------------------------------------------------- #
#                        Services
# --------------------------------------------------------- #


services:

  wiki:
    image: imladrislanvollon/wiki-postgres:latest
    restart: unless-stopped
    volumes:
      - images:/var/www/html/images
      - images-discret:/var/www/data/suppr
      - wiki-modules:/var/www/html/extensions:ro
      - logs:/var/www/data/logs
      - ./wiki/LocalSettings.php:/var/www/html/LocalSettings.php:ro
      - ./wiki/php.ini:/usr/local/etc/php/php.ini:ro
    env_file:
      - ./wiki.env
    environment:
      WIKI_URL:
      WIKI_DB_URL: db-wiki
      WIKI_DB_PORT:
      WIKI_DB_NAME_FILE: /run/secrets/db_name-wiki
      WIKI_DB_USER_FILE: /run/secrets/db_util-wiki
      WIKI_DB_PASSWORD_FILE: /run/secrets/db_pw-wiki
      WIKI_DB_SCHEMA: mediawiki
    networks:
       - traefik
       - reseau_interne
       - sortie # nécessaire pour les connexions smtp
    labels:
      - traefik.enable=true
    depends_on:
      docker-db:
        condition: service_healthy
    secrets: # secrets utilisés pour la configuration du compte admin du wiki
      - db_name-wiki
      - db_util-wiki
      - db_pw-wiki

  docker-db:
    image: postgres:15.4-alpine
    restart: unless-stopped
    environment:
      POSTGRES_PASSWORD_FILE: /run/secrets/db_pw
      POSTGRES_USER_FILE: /run/secrets/db_util
      POSTGRES_DB_FILE: /run/secrets/db_name
      CRON_CONFIG: "wiki:WIKI_DB_URL,WIKI_DB_NAME_FILE,WIKI_DB_USER_FILE,WIKI_DB_PASSWORD_FILE"
      WIKI_DB_URL: db-wiki
      WIKI_DB_PASSWORD_FILE: /run/secrets/db_pw-wiki
      WIKI_DB_USER_FILE: /run/secrets/db_util-wiki
      WIKI_DB_NAME_FILE: /run/secrets/db_name-wiki
    volumes:
      - ./db/:/docker-entrypoint-initdb.d   #script d'initialisation des bases de données
      - db:/var/lib/postgresql/data # volume contenant les données de la base
    networks:
      reseau_interne:
        aliases:
          - db-wiki # permet de simuler un sgbd différents par base de données
    secrets:
      - db_pw
      - db_util
      - db_name
      - db_pw-wiki
      - db_util-wiki
      - db_name-wiki
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U $$(cat $$WIKI_DB_USER_FILE) -d $$(cat $$WIKI_DB_NAME_FILE)"]
      interval: 10s
      timeout: 3s
      retries: 3

  cron:
    image: imladrislanvollon/cron-postgres:latest
    build:
      context: ./cron
      dockerfile: DockerFile
    depends_on:
      docker-db:
        condition: service_healthy
    env_file:
      - ./cron.git.env
    environment:
      CRON_CONFIG: "wiki:WIKI_DB_URL,WIKI_DB_NAME_FILE,WIKI_DB_USER_FILE,WIKI_DB_PASSWORD_FILE"
      WIKI_DB_URL: db-wiki
      POSTGRES_PASSWORD_FILE: /run/secrets/db_pw
      POSTGRES_USER_FILE: /run/secrets/db_util
      POSTGRES_DB_FILE: /run/secrets/db_name
      WIKI_DB_PASSWORD_FILE: /run/secrets/db_pw-wiki
      WIKI_DB_USER_FILE: /run/secrets/db_util-wiki
      WIKI_DB_NAME_FILE: /run/secrets/db_name-wiki
    volumes:
      - db_svg:/backup/
    networks:
      - reseau_interne
    secrets:
      - db_pw
      - db_util
      - db_name
      - db_pw-wiki
      - db_util-wiki
      - db_name-wiki
